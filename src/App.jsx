import GalleryList from "./components/GalleryList";
import Search from "./components/Search";
import "./assets/scss/main.scss";
function App() {
  return (
    <div>
      <header className="header">
        <h1>Gallery App</h1>
        <Search />
      </header>
      <GalleryList />
    </div>
  );
}

export default App;
