import { useEffect, useState } from "react";
import GalleryList from "./GalleryList";
import { galleryList } from "../constant";

function Search() {
  const [search, setSearch] = useState("");
  const handleChange = (e) => {
    setSearch(e.target.value);
  };

  //component lifecycle

  useEffect(() => {
    console.log("Good morning");
  }, []);

  return (
    <div>
      <input placeholder="Search" type="text" onChange={handleChange} />
    </div>
  );
}

export default Search;
