import { useEffect, useState } from "react";
import GalleryItem from "./GalleryItem";
import axios from "axios";

function GalleryList(props) {
  const [list, setList] = useState([]);
  const [loading, setLoading] = useState(false);

  const getPhotosList = () => {
    try {
      setLoading(true);
      axios({
        method: "GET",
        url: "https://jsonplaceholder.typicode.com/photos/",
      }).then((response) => {
        setList(response.data);
        setLoading(false);
      });
    } catch (error) {
      console.log(error, "error handling");
    }
  };

  useEffect(() => {
    getPhotosList();
  }, []);

  if (loading) {
    return <h1>Loading</h1>;
  }

  return (
    <ul className="gallery-list">
      {list.map((item) => {
        return <GalleryItem key={item.title} {...item} />;
      })}
    </ul>
  );
}

export default GalleryList;
