function GalleryItem(props) {
  const { title, thumbnailUrl } = props;

  const handleClick = () => {
    console.log("add to favorite");
  };
  return (
    <div className="gallery-item">
      <img src={thumbnailUrl} alt={title} />
      <h5>{title}</h5>
      <button onClick={handleClick}>Add to favorite</button>
    </div>
  );
}

export default GalleryItem;
